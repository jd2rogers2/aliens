import sys

import pygame

from modules.ship import Ship
from constants import *
from modules.bullet import Bullet

class Game:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((settings['screen_width'], settings['screen_height']))
        pygame.display.set_caption("Alien Invasion")

        self.ship = Ship(self)

    def run_game(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        self.ship.moving = DIRECTIONS['right']
                    if event.key == pygame.K_LEFT:
                        self.ship.moving = DIRECTIONS['left']
                    if event.key == pygame.K_UP:
                        self.ship.moving = DIRECTIONS['up']
                    if event.key == pygame.K_DOWN:
                        self.ship.moving = DIRECTIONS['down']
                    if event.key == pygame.K_SPACE:
                        Bullet(self)
                    if event.key == pygame.K_q:
                        sys.exit()
                elif event.type == pygame.KEYUP:
                    if (
                        event.key == pygame.K_RIGHT
                        or event.key == pygame.K_LEFT
                        or event.key == pygame.K_UP
                        or event.key == pygame.K_DOWN
                    ):
                        self.ship.moving = False

            self.screen.fill(settings['bg_color'])
            self.ship.update()
            self.ship.blitme()

            pygame.display.flip()