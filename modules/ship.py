import pygame

from constants import DIRECTIONS
class Ship:
    def __init__(self, ai_game):
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()

        self.image = pygame.image.load('./images/goodship.png')
        self.rect = self.image.get_rect()
        self.rect.midbottom = self.screen_rect.midbottom

        self.moving = False

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def update(self):
        if self.moving:
            # print(self.rect.midtop)
            if self.moving == DIRECTIONS['up'] and self.rect.top > 0:
                self.rect.y -= 1
            if self.moving == DIRECTIONS['down'] and self.rect.bottom < self.screen_rect.bottom:
                self.rect.y += 1
            if self.moving == DIRECTIONS['left'] and self.rect.left > 0:
                self.rect.x -= 1
            if self.moving == DIRECTIONS['right'] and self.rect.right < self.screen_rect.right:
                self.rect.x += 1
