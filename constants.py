settings = {
    'screen_width': 1200,
    'screen_height': 800,
    'bg_color': (230, 230, 230),

    'bullet_width': 3,
    'bullet_height': 15,
    'bullet_color': (60, 60, 60),
}

DIRECTIONS = {
    'right': 'right',
    'left': 'left',
    'up': 'up',
    'down': 'down',
}
