import pygame
from pygame.sprite import Sprite

from constants import settings

class Bullet(Sprite):
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.color = settings['bullet_color']
        self.rect = pygame.Rect(0, 0, settings['bullet_width'], settings['bullet_height'])
        self.rect.midtop = ai_game.ship.rect.midtop
        self.y = self.rect.y

        self.move()

    def move(self):
        self.rect.y -= 1
        self.y -= 1
        pygame.draw.rect(self.screen, self.color, self.rect)

        print('self', self)
        print('self.rect', self.rect)
        print('self.rect.y', self.rect.y)

        # if self.rect.top :
        #     self.move()